import axios from 'axios'

export class APIService {
  constructor () {
    this.instance = axios.create({
      baseURL: 'http://localhost:3000'
    })
  }

  getProjects () {
    return this.instance.get('/projects')
  }

  getProject (id) {
    return this.instance.get('/project/' + id)
  }
}
