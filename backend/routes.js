async function routes (fastify, options) {
    //project
    fastify.get('/projects', (request, reply) => {
        reply.send([{name: 'hej', id: 1}])
    })
    fastify.get('/project/:id', (request, reply) => {
        reply.send({name: 'hej', id: 1})
    })
    fastify.post('/project/', (request, reply) => {
        reply.send("project post success! " + request.body)
    })
    fastify.patch('/project/:id', (request, reply) => {
        reply.send("patching project: " + request.params.id)
    })

    //user
    fastify.get('/users/', (request, reply) => {
        reply.send([{name: 'Erik', id: 1}])
    })
    fastify.get('/user/:id', (request, reply) => {
        reply.send({name: 'Erik', id: 1})
    })
    fastify.post('/user/', (request, reply) => {
        reply.send("user post success! " + request.body)
    })
    fastify.patch('/user/:id', (request, reply) => {
        reply.send("patching user: " + request.params.id)
    })

    //techtag
    fastify.get('/techtags', (request, reply) => {
        reply.send([{name: 'Node.js', id: 1}])
    })
    fastify.get('/techtag/:id', (request, reply) => {
        reply.send({name: 'Node.js', id: 1})
    })
    fastify.post('/techtag/', (request, reply) => {
        reply.send("techtag post success! " + request.body)
    })
    fastify.patch('/techtag/:id', (request, reply) => {
        reply.send("patching techtag "+ request.params.id)
    })


    //progress
    fastify.get('/progress/:projectId', (request, reply) => {
        reply.send({name: 'Node.js', id: 1, status: 1})
    })


}

module.exports = routes;